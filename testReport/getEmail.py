__author__ = 'Administrator'
import os
import configparser

def read_email(f="email.ini"):
    config = configparser.ConfigParser()
    config.read(f)
    to_addr = eval(config['DEFAULT']['to_addr'])
    mail_host = config['DEFAULT']['mail_host']
    mail_user = config['DEFAULT']['mail_user']
    mail_pass = config['DEFAULT']['mail_pass']
    port = config['DEFAULT']['port']
    result = to_addr, mail_host, mail_user, mail_pass, port
    return result
