# -*- coding:utf-8 -*-
__author__ = 'shikun'
# web测试信息
class getDevices():
    def __init__(self,server_url='http://198.0.0.1:4444/wd/hub', platform='windows', version='', browser='firefox'):
        self.server_url = server_url
        self.platform = platform
        self.version = version
        self.browser = browser

    def get_server_url(self):
        return self.server_url
    def set_server_url(self, server_url):
        self.server_url = server_url

    def get_platform(self):
        return self.platform
    def set_platform(self, platform):
        self.platform = platform

    def get_version(self):
        return self.version
    def set_version(self, version):
        self.version = version

    def get_browser(self):
        return self.browser
    def set_browser(self, browser):
        self.browser = browser


